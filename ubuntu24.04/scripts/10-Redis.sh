#!/bin/bash

# 10-Redis

if [ "$redis_version" = "1" ];then # 1-) Redis (Official Package)
    sudo apt -y install redis
elif [ "$redis_version" = "2" ];then # 2-) Redis Latest (With Snap)
    sudo apt -y install snapd
    snap install redis
elif [ "$redis_version" = "3" ];then # 3-) Redis Latest (Compile From Source)
    wget -O /root/Downloads/redis-stable.tar.gz https://download.redis.io/redis-stable.tar.gz
    sudo mkdir -pv /root/Downloads/redis-stable
    tar -xvf /root/Downloads/redis-stable.tar.gz -C /root/Downloads/redis-stable --strip-components 1
    cd /root/Downloads/redis-stable || exit 2
    make -j "${core:=}" && make -j "$core" install
    sudo mkdir -pv /etc/redis/
    sudo mv -v /root/Downloads/redis-stable/redis.conf /etc/redis/redis.conf
    sed -i -e "s/# supervised auto/supervised systemd/g" /etc/redis/redis.conf
    sed -i -e "s/dir ./dir \/var\/lib\/redis/g" /etc/redis/redis.conf
    echo "[Unit]
Description=Redis In-Memory Data Store
After=network.target

[Service]
User=redis
Group=redis
ExecStart=/root/Downloads/redis-stable/src/redis-server /etc/redis/redis.conf
ExecStop=/root/Downloads/redis-stable/src/redis-cli shutdown
Restart=always

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/redis.service
    sudo adduser --system --group --no-create-home redis
    sudo mkdir -pv /var/lib/redis
    sudo chown redis:redis /var/lib/redis
    sudo chmod 770 /var/lib/redis
    sudo systemctl restart redis
    sudo systemctl enable redis
else
    echo "Out of options please choose between 1-3"
    sleep 2
fi