#!/bin/bash

# 4-Apache

if [ "$apache_version" = "1" ];then # 1-) Apache (From Official Package)
    sudo apt -y install apache2
    sudo systemctl enable apache2
    sudp systemctl start apache2
elif [ "$apache_version" = "2" ];then # 2-) Apache Latest (From Ondrej PPA)
    sudo add-apt-repository -y ppa:ondrej/apache2
    sudo apt -y install apache2
    sudo systemctl enable apache2
    sudp systemctl start apache2
else
    echo "Out of options please choose between 1-2"
    sleep 2
fi