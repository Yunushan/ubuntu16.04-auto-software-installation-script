#!/bin/bash

# 1-Php

if [ "$php_version" = "1" ];then # 1-) PHP 8.3 (Ubuntu Official Package)
    sudo apt -y install php8.3 php8.3-dev php8.3-mysql php8.3-bcmath php8.3-bz2 php8.3-cgi php8.3-cli \
    php8.3-common php8.3-curl php8.3-fpm php8.3-gnupg php8.3-imagick php8.3-memcache php8.3-memcached \
    php8.3-mongodb php8.3-odbc php8.3-opcache php8.3-pgsql php8.3-redis php8.3-soap php8.3-sqlite3 \
    php8.3-xml php8.3-yac php8.3-yaml php8.3-zip
    systemctl restart php8.3-fpm.service
    systemctl enable php8.3-fpm.service
elif [ "$php_version" = "2" ];then # 2-) PHP 8.4 (Ondrej PPA)
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php8.4 php8.4-dev php8.4-mysql php8.4-bcmath php8.4-bz2 php8.4-cgi php8.4-cli \
    php8.4-common php8.4-curl php8.4-fpm php8.4-gnupg php8.4-imagick php8.4-memcache php8.4-memcached \
    php8.4-mongodb php8.4-odbc php8.4-opcache php8.4-pgsql php8.4-redis php8.4-soap php8.4-sqlite3 \
    php8.4-xml php8.4-yac php8.4-yaml php8.4-zip
    systemctl restart php8.4-fpm.service
    systemctl enable php8.4-fpm.service
elif [ "$php_version" = "3" ];then # 3-) PHP 8.3 (Ondrej PPA)
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php8.3 php8.3-dev php8.3-mysql php8.3-bcmath php8.3-bz2 php8.3-cgi php8.3-cli \
    php8.3-common php8.3-curl php8.3-fpm php8.3-gnupg php8.3-imagick php8.3-memcache php8.3-memcached \
    php8.3-mongodb php8.3-odbc php8.3-opcache php8.3-pgsql php8.3-redis php8.3-soap php8.3-sqlite3 \
    php8.3-xml php8.3-yac php8.3-yaml php8.3-zip
    systemctl restart php8.3-fpm.service
    systemctl enable php8.3-fpm.service
elif [ "$php_version" = "4" ];then # 4-) PHP 8.2 (Ondrej PPA)
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php8.2 php8.2-dev php8.2-mysql php8.2-bcmath php8.2-bz2 php8.2-cgi php8.2-cli \
    php8.2-common php8.2-curl php8.2-fpm php8.2-memcache php8.2-mongodb php8.2-odbc php8.2-opcache \
    php8.2-pgsql php8.2-redis php8.2-soap php8.2-sqlite3 php8.2-xml php8.2-yac php8.2-yaml php8.2-zip
    systemctl restart php8.2-fpm.service
    systemctl enable php8.2-fpm.service
elif [ "$php_version" = "5" ];then # 5-) PHP 8.1 (Ondrej PPA)
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php8.1 php8.1-dev php8.1-mysql php8.1-bcmath php8.1-bz2 php8.1-cgi php8.1-cli \
    php8.1-common php8.1-curl php8.1-fpm php8.1-memcache php8.1-mongodb php8.1-odbc php8.1-opcache \
    php8.1-pgsql php8.1-redis php8.1-soap php8.1-sqlite3 php8.1-xml php8.1-yac php8.1-yaml php8.1-zip
    systemctl restart php8.1-fpm.service
    systemctl enable php8.1-fpm.service
elif [ "$php_version" = "6" ];then # 6-) PHP 8.0 (Ondrej PPA)
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php8.0 php8.0-dev php8.0-mysql php8.0-bcmath php8.0-bz2 php8.0-cgi php8.0-cli \
    php8.0-common php8.0-curl php8.0-fpm php8.0-memcache php8.0-mongodb php8.0-odbc php8.0-opcache \
    php8.0-pgsql php8.0-redis php8.0-soap php8.0-sqlite3 php8.0-xml php8.0-yac php8.0-yaml php8.0-zip
    systemctl restart php8.0-fpm.service
    systemctl enable php8.0-fpm.service
elif [ "$php_version" = "7" ];then # 7-) PHP 7.4 (Ondrej PPA)
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php7.4 php7.4-dev php7.4-mysql php7.4-bcmath php7.4-bz2 php7.4-cgi php7.4-cli \
    php7.4-common php7.4-curl php7.4-fpm php7.4-mongodb php7.4-odbc php7.4-opcache php7.4-pgsql \
    php7.4-redis php7.4-soap php7.4-sqlite3 php7.4-xml php7.4-yac php7.4-zip
    systemctl restart php7.4-fpm.service
    systemctl enable php7.4-fpm.service
elif [ "$php_version" = "8" ];then # 8-) PHP 7.3 (Ondrej PPA)
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php7.3 php7.3-dev php7.3-mysql php7.3-bcmath php7.3-bz2 php7.3-cgi php7.3-cli \
    php7.3-common php7.3-curl php7.3-fpm php7.3-odbc php7.3-opcache php7.3-pgsql \
    php7.3-redis php7.3-soap php7.3-sqlite3 php7.3-xml php7.3-yac php7.3-zip
    systemctl restart php7.3-fpm.service
    systemctl enable php7.3-fpm.service
elif [ "$php_version" = "9" ];then # 9-) PHP 7.2 (Ondrej PPA)
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php7.2 php7.2-dev php7.2-mysql php7.2-bcmath php7.2-bz2 php7.2-cgi php7.2-cli \
    php7.2-common php7.2-curl php7.2-fpm php7.2-odbc php7.2-opcache php7.2-pgsql php7.2-redis php7.2-soap php7.2-sqlite3 \
    php7.2-xml php7.2-yac php7.2-zip
    systemctl restart php7.2-fpm.service
    systemctl enable php7.2-fpm.service
elif [ "$php_version" = "10" ];then # 10-) PHP 7.1 (Ondrej PPA)
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt -y install php7.1 php7.1-dev php7.1-mysql php7.1-bcmath php7.1-bz2 php7.1-cgi php7.1-cli \
    php7.1-common php7.1-curl php7.1-fpm php7.1-odbc php7.1-opcache php7.1-pgsql php7.1-soap php7.1-sqlite3 \
    php7.1-xml php7.1-yac php7.1-zip
    systemctl restart php7.1-fpm.service
    systemctl enable php7.1-fpm.service
elif [ "$php_version" = "11" ];then # 11-) PHP 7.0 (Ondrej PPA)
    sudo apt -y install php7.0 php7.0-dev php7.0-mysql php7.0-bcmath php7.0-bz2 php7.0-cgi php7.0-cli \
    php7.0-common php7.0-curl php7.0-fpm php7.0-odbc php7.0-opcache php7.0-pgsql php7.0-soap php7.0-sqlite3 \
    php7.0-xml php7.0-yac php7.0-zip
    systemctl restart php7.0-fpm.service
    systemctl enable php7.0-fpm.service
elif [ "$php_version" = "12" ];then # 12-) PHP 5.6 (Ondrej PPA)
    sudo apt -y install php5.6 php5.6-dev php5.6-mysql php5.6-bcmath php5.6-bz2 php5.6-cgi php5.6-cli \
    php5.6-common php5.6-curl php5.6-fpm php5.6-odbc php5.6-opcache php5.6-pgsql php5.6-soap php5.6-sqlite3 \
    php5.6-xml php5.6-zip
    systemctl restart php5.6-fpm.service
    systemctl enable php5.6-fpm.service
else
    echo "Out of options please choose between 1-12"
    sleep 2
fi