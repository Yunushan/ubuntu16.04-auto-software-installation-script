#!/bin/bash

# 5-FFmpeg

if [ "$ffmpeg_version" = "1" ];then # 1-) FFmpeg (From Official Package)
    sudo apt -y install ffmpeg
elif [ "$ffmpeg_version" = "2" ];then # 2-) FFmpeg (From savoury1 PPA)
    sudo add-apt-repository -y ppa:savoury1/ffmpeg4
    sudo apt -y install ffmpeg
elif [ "$ffmpeg_version" = "3" ];then # 3-) FFmpeg (From Snap)
    sudo apt -y install snapd
    printf "\nPlease Choose Your Desired Snap FFmpeg Version\n\n1-) FFmpeg (Snap Stable)\n\
2-) FFmpeg (From savoury1 PPA)\n\nPlease Select FFmpeg Version:"
    read -r snap_ffmpeg_version
    if [ "$snap_ffmpeg_version" = "1" ];then # 1-) FFmpeg (Snap Stable)
        sudo snap install ffmpeg
    elif [ "$snap_ffmpeg_version" = "2" ];then # 2-) FFmpeg (From savoury1 PPA)
         sudo snap install ffmpeg --edge
    else 
        echo "Out of options please choose between 1-2"
    fi
else
    echo "Out of options please choose between 1-3"
    sleep 2
fi
