#!/bin/bash

# Global Variables
cpuarch=$(uname -m)
superuser=$(getent group sudo | cut -d: -f4)
codename=$(lsb_release -cs)
scripts_path=$(find / -name scripts | grep -i "ubuntu22.04/scripts" | head -n 1)
core=$(nproc)
# Select Which Softwares to be Installed

choice () {
    local choice=$1
    if [[ ${opts[choice]} ]] # toggle
    then 
        opts[choice]=
    else
        opts[choice]=+
    fi
}
PS3='Please enter your choice(s): '
while :
do
    clear
    options=("PHP ${opts[1]}" "Nginx ${opts[2]}" "Grub Customizer ${opts[3]}" "Apache ${opts[4]}" "FFmpeg ${opts[5]}" 
    "WineHQ ${opts[6]}" "Nmap ${opts[7]}" "OpenSSH ${opts[8]}" "OpenSSL ${opts[9]}" "Redis ${opts[10]}" "Jenkins ${opts[11]}"
    "Docker ${opts[12]}" "Done ${opts[13]}")
    select opt in "${options[@]}"
    do
        case $opt in
            "PHP ${opts[1]}")
                choice 1
                break
                ;;
            "Nginx ${opts[2]}")
                choice 2
                break
                ;;
            "Grub Customizer ${opts[3]}")
                choice 3
                break
                ;;
            "Apache ${opts[4]}")
                choice 4
                break
                ;;
            "FFmpeg ${opts[5]}")
                choice 5
                break
                ;;
            "WineHQ ${opts[6]}")
                choice 6
                break
                ;;
            "Nmap ${opts[7]}")
                choice 7
                break
                ;;
            "OpenSSH ${opts[8]}")
                choice 8
                break
                ;;
            "OpenSSL ${opts[9]}")
                choice 9
                break
                ;;
            "Redis ${opts[10]}")
                choice 10
                break
                ;;
            "Jenkins ${opts[11]}")
                choice 11
                break
                ;;
            "Docker ${opts[12]}")
                choice 12
                break
                ;;
            "Done ${opts[13]}")
                break 2
                ;;
            *) printf '%s\n' 'Please Choose Between 1-13';;
        esac
    done
done

printf '%s\n\n' 'Options chosen:'
for opt in "${!opts[@]}"
do
    if [[ ${opts[opt]} ]]
    then
        printf '%s\n' "Option $opt"
    fi
done

if [ "${opts[opt]}" = "" ]
then
    exit
fi

# Loading Bar

printf "Installation starting"
value=0
while [ $value -lt 600 ]
do
    value=$((value+20))
    printf "."
    sleep 0.05
done
printf "\n"

#Install Necessary Packages

sudo apt update
sudo apt -y install bash-completion net-tools wget curl lynx
source /etc/profile.d/bash_completion.sh
grep -qxF 'source /etc/profile.d/bash_completion.sh' ~/.bashrc || echo 'source /etc/profile.d/bash_completion.sh' >> ~/.bashrc

# Create Downloads folder if doesn't exist
if [ -d "/root/Downloads/" ]
then
    :
else
    sudo mkdir -pv /root/Downloads/
fi

# INSTALLATION BY SELECTION
for opt in "${!opts[@]}"
do
    if [[ ${opts[opt]} ]]
    then
        case $opt in 

            1) 
            #PHP
            . "$scripts_path/1-Php.sh"
            printf "\nPhp Installation Has Finished\n\n"
            ;;
            2)
            # 2- Nginx
            . "$scripts_path/2-Nginx.sh"
            printf "\nNginx Installation Has Finished\n\n"
            ;;
            3)
            # 3- Grub Customizer
            . "$scripts_path/3-Grub-Customizer.sh"
            printf "\nGrub Customizer Installation Has Finished\n\n"
            ;;
            4)
            # 4- Apache
            . "$scripts_path/4-Apache.sh"
            printf "\nApache Installation Has Finished\n\n"
            ;;
            5)
            # 5- FFmpeg
            . "$scripts_path/5-FFmpeg.sh"
            printf "\nFFmpeg Installation Has Finished\n\n"
            ;;
            6)
            # 6- WineHQ
            . "$scripts_path/6-WineHQ.sh"
            printf "\nWineHQ Installation Has Finished\n\n"
            ;;
            7)
            # 7- Nmap
            . "$scripts_path/7-Nmap.sh"
            printf "\nNmap Installation Has Finished\n\n"
            ;;
            8)
            # 8- OpenSSH
            . "$scripts_path/8-OpenSSH.sh"
            printf "\nOpenSSH Installation Has Finished\n\n"
            ;;
            9)
            # 9- OpenSSL
            . "$scripts_path/9-OpenSSL.sh"
            printf "\nOpenSSL Installation Has Finished\n\n"
            ;;
            10)
            # 10- Redis
            . "$scripts_path/10-Redis.sh"
            printf "\nRedis Installation Has Finished\n\n"
            ;;
            11)
            # 11- Jenkins
            . "$scripts_path/11-Redis.sh"
            printf "\nRedis Installation Has Finished\n\n"
            ;;
            12)
            # 12- Docker
            . "$scripts_path/12-Docker.sh"
            printf "\nDocker Installation Has Finished\n\n"
            ;;
        esac
    fi
done