#!/bin/bash

#8-OpenSSH installer.sh
printf "\nPlease Choose Your Desired OpenSSH Version\n\n1-) OpenSSH (Official Package)\n\
2-) OpenSSH Latest (Compile From Source)\n\nPlease Select Your OpenSSH Version:"
read -r openssh_version
if [ "$openssh_version" = "1" ];then
    sudo apt -y install openssh-server
elif [ "$openssh_version" = "2" ];then
    sudo apt -y install lynx libpam-dev libpam-modules build-essential zlib1g-dev libssl-dev libpam0g-dev libselinux1-dev
    openssh_latest=$(lynx -dump https://www.openssh.com/releasenotes.html | awk {'print $2'} | grep -i p1.tar.gz | head -n 1)
    wget -O /root/Downloads/openssh-latest.tar.gz "$openssh_latest"
    sudo mkdir -pv /root/Downloads/openssh-latest
    tar -xvf /root/Downloads/openssh-latest.tar.gz -C /root/Downloads/openssh-latest --strip-components 1
    sudo mkdir -pv /var/lib/sshd
    sudo chmod -R 700 /var/lib/sshd/
    sudo chown -R root:sys /var/lib/sshd/
    sudo useradd -r -U -d /var/lib/sshd/ -c "sshd privsep" -s /bin/false sshd
    cd /root/Downloads/openssh-latest
    ./configure --with-md5-passwords \
                --with-pam \
                --with-selinux \
                --with-privsep-path=/var/lib/sshd/ \
                --sysconfdir=/etc/ssh
    make -j "$core" && make -j "$core" install
else
    echo "Out of options please choose between 1-2"
fi