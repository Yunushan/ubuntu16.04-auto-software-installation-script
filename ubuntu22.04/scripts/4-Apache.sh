#!/bin/bash

#4-Apache installer.sh
printf "\nPlease Choose Your Desired Apache Version\n\n1-) Apache (From Official Package)\n\
2-) Apache Latest (From Ondrej PPA)\n\nPlease Select Apache Version:"
read -r apache_version
if [ "$apache_version" = "1" ];then
    sudo apt -y install apache2
    sudo systemctl enable apache2
    sudp systemctl start apache2
elif [ "$apache_version" = "2" ];then
    sudo add-apt-repository -y ppa:ondrej/apache2
    sudo apt -y install apache2
    sudo systemctl enable apache2
    sudp systemctl start apache2
else
    echo "Out of options please choose between 1-2"
fi