#!/bin/bash

#9-OpenSSL installer.sh
printf "\nPlease Choose Your Desired OpenSSL Version\n\n1-) OpenSSL (From Official Package)\n\
2-) OpenSSL 1.1 Latest (Compile From Source)\n3-) OpenSSL 3.0 Latest (Compile From Source)\n\
\n\nPlease Select Your OpenSSL Version:"
read -r openssl_version
if [ "$openssl_version" = "1" ];then
    sudo apt -y install openssl
elif [ "$openssl_version" = "2" ];then
    sudo apt -y install lynx perl gcc build-essential checkinstall zlib1g-dev
    openssl1_latest=$(lynx -dump https://www.openssl.org/source/ | awk '/http/{print $2}' | grep -iv '.asc\|sha\|fips' \
    | grep -i .tar.gz | head -n 1)
    wget -O /root/Downloads/openssl1_latest.tar.gz "$openssl1_latest"
    sudo mkdir -pv /root/Downloads/openssl1-latest
    tar -xvf /root/Downloads/openssl1_latest.tar.gz  -C /root/Downloads/openssl1-latest --strip-components 1
    cd /root/Downloads/openssl1-latest
    ./config    --prefix=/usr/local/ssl \
                --openssldir=/usr/local/ssl \
                shared \
                zlib

    make -j "$core" && make -j "$core" install
    echo "export PATH="/usr/local/ssl/bin:"${PATH}""" >> ~/.bashrc
    source /root/.bashrc
    openssl_conf_location=$(cat /etc/ld.so.conf.d/openssl-1* | head -n 1)
    echo "/usr/local/ssl/lib" >> "$openssl_conf_location"
    sudo ldconfig -v
elif [ "$openssl_version" = "3" ];then
    sudo apt -y install lynx perl gcc build-essential checkinstall zlib1g-dev
    openssl3_latest=$(lynx -dump https://www.openssl.org/source/ | awk '/http/{print $2}' | grep -iv '.asc\|sha\|fips' \
    | grep -i .tar.gz | tail -n 1)
    wget -O /root/Downloads/openssl3_latest.tar.gz "$openssl3_latest"
    sudo mkdir -pv /root/Downloads/openssl3-latest
    tar -xvf /root/Downloads/openssl3_latest.tar.gz  -C /root/Downloads/openssl3-latest --strip-components 1
    cd /root/Downloads/openssl3-latest
    ./config
    make -j "$core" && make -j "$core" install
    echo "export PATH="/usr/local/ssl/bin:"${PATH}""" >> ~/.bashrc
    ln -s /usr/local/lib64/libssl.so.3 /usr/lib64/libssl.so.3
    ln -s /usr/local/lib64/libcrypto.so.3 /usr/lib64/libcrypto.so.3
    sudo ldconfig -v
else
    echo "Out of options please choose between 1-3"
fi